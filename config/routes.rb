Rails.application.routes.draw do

	ActiveAdmin.routes(self)
	root to: "enterprises#index"

	devise_for :users
	resources :enterprises
	resources :dishes
	resources :users

end

