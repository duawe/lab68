class UsersController < ApplicationController

	before_action :find_user, only: [:show, :edit, :update, :destroy]

	def index
		@users = User.all
	end
	def show

	end
	def edit

	end
	def update
		if @user.update(user_params)
			upload_picture
			redirect_to user_path
		else
			render 'edit'
		end
	end
	def destroy
		@user.destroy
		redirect_to users_path 
	end

	private

	def find_user
		@user = User.find(params[:id])
	end
	def user_params
		params.require(:user).permit(:name, :email, :password, :password_confirmation, :address)
	end
	def upload_picture
		@user.picture.attach(uploaded_file) if uploaded_file.present?
	end

	def uploaded_file
		params[:user][:picture]
	end
end
