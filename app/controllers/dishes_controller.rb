class DishesController < ApplicationController

	before_action :find_dish, only: [:show, :edit, :update, :destroy]

	def index
		@dishes = Dish.all
	end
	def new
		@dish = Dish.new
	end	
	def show

	end
	def create
		@dish = Dish.new(dish_params)
		
		if @dish.save
			upload_picture
			redirect_to dishes_path
		else
			render 'new'
		end
	end
	def edit

	end
	def update
		if @dish.update(dish_params)
			upload_picture
			redirect_to dish_path
		else
			render 'edit'
		end
	end
	def destroy
		@dish.destroy
		redirect_to dishes_path 
	end

	private

	def find_dish
		@dish = Dish.find(params[:id])
	end
	def dish_params
		params.require(:dish).permit(:name, :description, :price)
	end
	def upload_picture
		@dish.picture.attach(uploaded_file) if uploaded_file.present?
	end

	def uploaded_file
		params[:dish][:picture]
	end
end
