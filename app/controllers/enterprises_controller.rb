class EnterprisesController < ApplicationController

	before_action :find_enterprise, only: [:show, :edit, :update, :destroy]

	def index
		@enterprises = Enterprise.all
	end
	def new
		@enterprise = Enterprise.new
	end	
	def show

	end
	def create
		@enterprise = Enterprise.new(enterprise_params)
		
		if @enterprise.save
			upload_picture
			redirect_to enterprises_path
		else
			render 'new'
		end
	end
	def edit

	end
	def update
		if @enterprise.update(enterprise_params)
			upload_picture
			redirect_to enterprise_path
		else
			render 'edit'
		end
	end
	def destroy
		@enterprise.destroy
		redirect_to enterprises_path 
	end

	private

	def find_enterprise
		@enterprise = Enterprise.find(params[:id])
	end
	def enterprise_params
		params.require(:enterprise).permit(:name, :dish_id, :description)
	end
	def upload_picture
		@enterprise.picture.attach(uploaded_file) if uploaded_file.present?
	end

	def uploaded_file
		params[:enterprise][:picture]
	end
end
