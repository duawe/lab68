class Enterprise < ApplicationRecord
	has_many :dishes

	has_one_attached :picture
	validates :picture,  file_content_type: { allow: ['image/jpeg', 'image/gif', 'image/png'] }

	validates :name, presence: true,
	length: { maximum: 250 }

	validates :price, presence: true,
	numericality: { greater_than_or_equal_to: 0 }
end
