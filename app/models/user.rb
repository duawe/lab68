class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_one_attached :picture
   # has_many_attached :pictures

  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :validatable
  validates :name, presence: true, length: { maximum: 50 }
end
