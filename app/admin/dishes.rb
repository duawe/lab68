ActiveAdmin.register Dish do

	form do |f|
		f.inputs do
			f.input :name
			f.input :description
			f.input :price
			if f.object.picture.attached?
				f.input :picture,
				:as => :file,
				:hint => image_tag(
					url_for(
						f.object.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
						)
					)
			else
				f.input :picture,
				:as => :file
			end
		end
		f.actions
	end

	index do
		selectable_column
		id_column
		column :picture do |dish|
			image_tag dish.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
		end
		column :name do |dish|
			link_to dish.title, admin_dish_path(dish)
		end
		column :description
		column :price
		actions
	end

	show do
		attributes_table do
			row :image do |dish|
				image_tag dish.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0' })
			end
			row :name
			row :description
			row :price
		end
		active_admin_comments
	end

	permit_params :name, :description, :picture, :price

end

