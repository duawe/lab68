class AddEnterpriseToDishes < ActiveRecord::Migration[5.2]
  def change
    add_reference :dishes, :enterprise, foreign_key: true
  end
end
